# README #

### What is this repository for? ###

* Project implements the [Tic-tac-toe](https://en.wikipedia.org/wiki/Tic-tac-toe) game.

### Dependencies ###

* [CMake](https://cmake.org/)
* [Visual Studio 2015](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx)