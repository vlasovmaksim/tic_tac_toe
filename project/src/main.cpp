#include "tic_tac_toe.h"
#include <iostream>


int main() {

  // Create game.
  TicTacToe game;

  // Init game.
  game.init();

  // Main loop.
  while (true) {
    if (!game.move()) {
      break;
    }
  }

  game.finish();

  return 0;
}

