#pragma once

#include <iostream>
#include <sstream>
#include <vector>
#include "cell.h"


/// Main class. 
/// Describe TicTacToe game.
class TicTacToe {
public:
  TicTacToe() : size_(3) {
    std::cout << "Welcome to the TicTacToe game!" << std::endl;
  }

  ~TicTacToe() {}

  TicTacToe(const TicTacToe& rhs) {
    this->matrix_.assign(
      std::begin(rhs.matrix_),
      std::end(rhs.matrix_));

    this->size_ = rhs.size_;
    this->matrix_size_ = rhs.matrix_size_;
    this->cell_number_ = -1;
    this->player_cell_value_ = rhs.player_cell_value_;
    this->ai_cell_value_ = rhs.ai_cell_value_;
    this->is_stop_ = rhs.is_stop_;
    this->count_ = rhs.count_;
    this->result_ << rhs.result_.str();
  }

  TicTacToe& operator=(const TicTacToe& rhs) {
    this->matrix_.assign(
      std::begin(rhs.matrix_),
      std::end(rhs.matrix_));

    this->size_ = rhs.size_;
    this->matrix_size_ = rhs.matrix_size_;
    this->cell_number_ = -1;
    this->player_cell_value_ = rhs.player_cell_value_;
    this->ai_cell_value_ = rhs.ai_cell_value_;
    this->is_stop_ = rhs.is_stop_;
    this->count_ = rhs.count_;
    this->result_ << rhs.result_.str();

    return *this;
  }

  /// Initialize the class.
  void init();

  /// Initialize matrix.
  void zero();

  /// Contains the main logic of the game.
  bool move();

  /// Player and AI move.
  ///
  /// Updates the latest coordinates of the current cell.
  void player_move();
  void ai_move();

  /// It is called on every move 
  /// with last coordinates of a Cell.
  ///
  /// x, y - coordinates of a Cell (starts from 0).
  void check_winner();
  void check_winner(const int& x, const int& y);

  /// It stops console after the game end.
  void finish() const;

  /// Set cell.
  ///
  /// cell_number - index in the matrix from 0 to 8.
  /// value - empty, X, O.
  ///
  /// throw (std::out_of_range, std::invalid_argument)
  void set_cell(const int& cell_number,
                const Cell& value);

  /// Get cell by number or by index.
  Cell cell(const int& cell_number) const;
  Cell cell(const int& i, const int& j) const;

  bool is_stop() const;

  /// Print field to the console.
  friend void operator<<(std::ostream& out,
                         const TicTacToe& field);

private:
  /// Store field.
  std::vector<Cell> matrix_;

  /// Side size.
  int size_ = 0;

  /// Full size.
  int matrix_size_ = 0;

  // Current cell number.
  int cell_number_ = -1;

  // Player cell value.
  Cell player_cell_value_ = Cell::Empty;

  /// AI cell value.
  Cell ai_cell_value_ = Cell::Empty;

  /// Is stop game flag.
  bool is_stop_ = false;

  /// Count all moves.
  size_t count_ = 0;

  /// Store the result of a game.
  std::stringstream result_;
};

