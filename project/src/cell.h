#pragma once

#include <ostream>


// Store one of three states: empty, X, O.
enum class Cell {
  Empty,
  Cross,
  Toe
};

inline std::ostream& operator<<(std::ostream& out,
                                const Cell& value) {

  if (value == Cell::Empty) {
    out << ".";
  } else if (value == Cell::Cross) {
    out << "X";
  } else if (value == Cell::Toe) {
    out << "O";
  }

  return out;
}
