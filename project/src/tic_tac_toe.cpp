#include "tic_tac_toe.h"
#include <iostream>
#include <ctime>


void TicTacToe::init() {
  // Set matrix size.
  matrix_size_ = pow(size_, 2);

  // Reserve vector space.
  matrix_.reserve(matrix_size_);

  // Set initial matrix values to Cell::Empty.
  zero();

  // Set initial seed to the current time.
  srand(time(nullptr));

  // Choose who is gonna start the game first:
  // a player or a computer.
  std::cout << "Start first? Enter 1 (Yes) or 0 (No):" << std::endl;

  // Loop until we get a valid input data.
  bool result = false;

  do {
    try {
      int tmp = -1;

      std::cin >> tmp;
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

      // Check input values.
      if (tmp == 0 || tmp == 1) {
        if (tmp == 1) {
          std::cout << "Player begins." << std::endl;

          player_cell_value_ = Cell::Cross;
        } else {
          std::cout << "AI begins." << std::endl;

          player_cell_value_ = Cell::Toe;
        }

        result = true;
      } else {
        throw std::invalid_argument("Wrong number, please try again!");
      }
    } catch (std::invalid_argument e) {
      std::cout << e.what() << std::endl;
    }
  } while (!result);

  // If a player choose the Cross 
  // then an AI chooses the Toe and otherwise. 
  player_cell_value_ == Cell::Cross
    ? ai_cell_value_ = Cell::Toe
    : ai_cell_value_ = Cell::Cross;
}

void TicTacToe::zero() {
  for (int i = 0; i < matrix_size_; i++) {
    matrix_.push_back(Cell::Empty);
  }
}

bool TicTacToe::move() {
  // Determine who is gonna move next: 
  // the player or the AI.
  switch ((count_ + int(ai_cell_value_)) % 2) {
    case 0:
      std::cout << "Player move: ";
      player_move();
      break;
    case 1:
      std::cout << "AI move: ";
      ai_move();
      break;
    default:
      break;
  }

  check_winner();

  // Print to the console.
  std::cout << *this;

  // Stop the game if needed.
  if (is_stop()) {
    std::cout << "End game: " << result_.str() << std::endl;
    return false;
  }

  count_++;

  return true;
}

void TicTacToe::player_move() {
  bool result = false;

  std::cout << "Please input cell number between 1 and 9!" << std::endl;

  do {
    try {
      // Get input data from user.
      std::cin >> cell_number_;
      std::cin.clear();
      std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

      // Enter input cell number 1 - 9 instead of 0 - 8.
      cell_number_--;

      set_cell(cell_number_, player_cell_value_);

      result = true;

    } catch (std::invalid_argument e) {
      std::cout << e.what() << std::endl;
    }
    catch (std::out_of_range e) {
      std::cout << e.what() << std::endl;
    }
  } while (!result);
}

void TicTacToe::ai_move() {
  bool result = false;

  do {
    try {
      // Get random number.
      cell_number_ = rand() % 9;

      set_cell(cell_number_, ai_cell_value_);

      result = true;

    } catch (std::invalid_argument e) {}
  } while (!result);
}

void TicTacToe::check_winner() {
  check_winner(cell_number_ / size_, cell_number_ % size_);
}

void TicTacToe::check_winner(const int& x, const int& y) {
  // Check horizontal, vertical, diagonal 
  // and anti-diagonal axis.

  // Get current cell value.
  Cell current_cell = cell(x, y);

  // Check horizontal axis.
  for (int i = 0; i < size_; i++) {
    if (cell(i, y) != current_cell) {
      break;
    }

    if (i == size_ - 1) {
      result_ << cell(x, y) << " win!" << std::endl;

      is_stop_ = true;
      return;
    }
  }

  // Check vertical axis.
  for (int i = 0; i < size_; i++) {
    if (cell(x, i) != current_cell) {
      break;
    }

    if (i == size_ - 1) {
      result_ << cell(x, y) << " win!" << std::endl;

      is_stop_ = true;
      return;
    }
  }

  // Check diagonal.
  if (x == y) {
    for (int i = 0; i < size_; i++) {
      if (cell(i, i) != current_cell) {
        break;
      }

      if (i == size_ - 1) {
        result_ << cell(x, y) << " win!" << std::endl;

        is_stop_ = true;
        return;
      }
    }
  }

  // Check anti-diagonal.
  for (int i = 0; i < size_; i++) {
    if (cell(i, (size_ - 1) - i) != current_cell) {
      break;
    }

    if (i == size_ - 1) {
      result_ << cell(x, y) << " win!" << std::endl;

      is_stop_ = true;
      return;
    }
  }

  // Check draw.
  if (count_ == matrix_size_ - 1) {
    result_ << "draw!" << std::endl;

    is_stop_ = true;
    return;
  }
}

void TicTacToe::set_cell(const int& cell_number, const Cell& value) {
  // Check index.
  if (cell_number < 0 || cell_number > 8) {
    throw std::out_of_range("Wrong cell number, please try again!");
  }

  // Check index value.
  if (matrix_.at(cell_number) != Cell::Empty) {
    throw std::invalid_argument("Wrong cell number, please try again!");
  }

  matrix_.at(cell_number) = value;
}

Cell TicTacToe::cell(const int& cell_number) const {
  return matrix_.at(cell_number);
}

Cell TicTacToe::cell(const int& i, const int& j) const {
  return cell(i * size_ + j);
}

bool TicTacToe::is_stop() const {
  return is_stop_;
}

void TicTacToe::finish() const {
  std::cout << "Press any key to continue..." << std::endl;

  std::cin.get();
}

void operator<<(std::ostream& out,
                const TicTacToe& field) {

  for (int i = 0; i < field.matrix_size_; i++) {
    if (i % field.size_ == 0) {
      std::cout << std::endl;
    }

    std::cout << field.cell(i);
  }

  std::cout << std::endl;

  std::cout << "*****" << std::endl;

  std::cout << std::endl;
}

